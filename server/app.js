var express = require('express');
var app = express();
var cors = require('cors');


app.use(cors());

app.get('/securized_page', function(req, res) {
    res.status(401);
    res.json({
        req_path: req.path,
        req_headers: req.headers,
        req_params: req.params || [],
        req_body: req.body || {}
    })
});

app.post('/securized_page', function(req, res) {
    res.status(401);
    res.json({
        req_path: req.path,
        req_headers: req.headers,
        req_params: req.params || {},
        req_body: req.body || {}
    })
});

app.listen(3000, function() {
  console.log('Aplicación ejemplo, escuchando el puerto 3000!');
});