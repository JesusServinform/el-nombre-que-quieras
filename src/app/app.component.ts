import { Component, ViewChild } from '@angular/core';

import { Platform, MenuController, Nav } from 'ionic-angular';

import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { Digest } from '../libs/digest';
import { HTTP } from '@ionic-native/http/ngx';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // make HelloIonicPage the root (or first) page
  rootPage = HelloIonicPage;
  pages: Array<{title: string, component: any}>;

  responses: string[];

  constructor(
    public platform: Platform,
    public menu: MenuController,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public http: HTTP,
    public digest: Digest
  ) {
    this.initializeApp();

    this.responses = [];

    // set our app's pages
    this.pages = [
      { title: 'Hello Ionic', component: HelloIonicPage },
      { title: 'My First List', component: ListPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // tests
      this.do_login_to_viasat();

      this.get_pokemon_ditto_info();
      this.get_pokemon_ditto_info_with_slash();

      this.post_reqres_in();

      this.get_local_server_unauthorized();
      this.post_local_server_unauthorized();
    })
  }

  async get_local_server_unauthorized() {

    this.http.get("http://localhost:3000/securized_page", { "id": "1" }, {})
      .then(x => console.log(x))
      .catch(x => {
        console.log(x)
      });

    this.http.get("http://localhost:3000/securized_page", { "id": "1" }, {})
      .then(x => console.log(x))
      .catch(x => {
        console.log(x)
      });
      
  }

  async post_local_server_unauthorized() {

    this.http.post("http://localhost:3000/securized_page", { id: 1 }, {})
      .then(x => console.log(x))
      .catch(x => {
        console.log(x)
      });

    this.http.post("http://localhost:3000/securized_page", { "id": 1 }, {})
      .then(x => console.log(x))
      .catch(x => {
        console.log(x)
      });
      
  }

  async do_login_to_viasat() {    
    
    console.log("Login to viasat");

    const data = {
      "version": "1.0.0.1",
      "action": "LoginUser",
      "appcode": "",
      "appver": "",
      "CheckAuthorization": true
    };   

    try {
      this.digest.request("DeviceCustomerDataService.svc/json/LoginUser", data)
      .then(x => {
        this.responses.push(JSON.stringify(x));
        console.log(x);
      })
      .catch(x => console.error(x));
    } catch {
      console.error("ROMPO PORQUE El MUNDO NO ME ENTIENDE :-(")
    }
    
  }

  async get_pokemon_ditto_info() {
    this.http.get("https://pokeapi.co/api/v2/pokemon/ditto", null, null)
      .then(x => {
        this.responses.push(JSON.stringify(x));
        console.log(x);
      })
      .catch(x => console.error(x));
  }

  async get_pokemon_ditto_info_with_slash() {
    this.http.get("https://pokeapi.co/api/v2/pokemon/ditto/", null, null)
      .then(x => {
        this.responses.push(JSON.stringify(x));
        console.log(x);
      })
      .catch(x => console.error(x));
  }
  
  async post_reqres_in() {
    const data = {
      name: "morpheus",
      job: "leader"
    };

    const headers = {
      "Content-type": "application/json",
    };

    this.http.post("https://reqres.in/api/users", data, headers)
      .then(x => {
        this.responses.push(JSON.stringify(x));
        console.log(x);
      })
      .catch(x => console.error(x));
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }
}
